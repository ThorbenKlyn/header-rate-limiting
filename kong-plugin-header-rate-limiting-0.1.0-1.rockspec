package = "kong-plugin-header-rate-limiting"  -- TODO: rename, must match the info in the filename of this rockspec!
                                  -- as a convention; stick to the prefix: `kong-plugin-`
version = "0.1.0-1"               -- TODO: renumber, must match the info in the filename of this rockspec!
-- The version '0.1.0' is the source code version, the trailing '1' is the version of this rockspec.
-- whenever the source version changes, the rockspec should be reset to 1. The rockspec version is only
-- updated (incremented) when this file changes, but the source remains the same.

supported_platforms = {"linux", "macosx"}
source = {
  url = "git+https://git.trkl.it/thor/header-rate-limiting.git",
  tag = "0.1.0"
}

description = {
  summary = "Kong is a scalable and customizable API Management Layer built on top of Nginx.",
  homepage = "http://getkong.org",
  license = "Apache 2.0"
}

dependencies = {
}

build = {
  type = "builtin",
  modules = {
    -- TODO: add any additional files that the plugin consists of
    ["kong.plugins.header-rate-limiting.migrations"] = "kong/plugins/header-rate-limiting/migrations/init.lua",
    ["kong.plugins.header-rate-limiting.migrations.000_base_rate_limiting"] = "kong/plugins/header-rate-limiting/migrations/000_base_rate_limiting.lua",
    ["kong.plugins.header-rate-limiting.migrations.003_10_to_112"] = "kong/plugins/header-rate-limiting/migrations/003_10_to_112.lua",
    ["kong.plugins.header-rate-limiting.migrations.004_200_to_210"] = "kong/plugins/header-rate-limiting/migrations/004_200_to_210.lua",
    ["kong.plugins.header-rate-limiting.expiration"] = "kong/plugins/header-rate-limiting/expiration.lua",
    ["kong.plugins.header-rate-limiting.handler"] = "kong/plugins/header-rate-limiting/handler.lua",
    ["kong.plugins.header-rate-limiting.schema"] = "kong/plugins/header-rate-limiting/schema.lua",
    ["kong.plugins.header-rate-limiting.daos"] = "kong/plugins/header-rate-limiting/daos.lua",
    ["kong.plugins.header-rate-limiting.policies"] = "kong/plugins/header-rate-limiting/policies/init.lua",
    ["kong.plugins.header-rate-limiting.policies.cluster"] = "kong/plugins/header-rate-limiting/policies/cluster.lua",

  }
}

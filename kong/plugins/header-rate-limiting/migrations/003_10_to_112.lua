return {
  postgres = {
    up = [[
      CREATE INDEX IF NOT EXISTS header_ratelimiting_metrics_idx ON header_ratelimiting_metrics (service_id, route_id, period_date, period);
    ]],
  },

  cassandra = {
    up = [[
    ]],
  },
}
